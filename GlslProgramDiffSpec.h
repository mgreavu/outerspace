#ifndef GLSLPROGRAMDIFFSPEC_H
#define GLSLPROGRAMDIFFSPEC_H

#include <graphics3d/GlslProgram.h>
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"

class GlslProgramDiffSpec : public GlslProgram
{
public:
    bool Init() override;

    void SetModel(const glm::mat4& model);
    void SetView(const glm::mat4& view);
    void SetProjection(const glm::mat4& projection);

    void SetViewPos(const glm::vec3& viewPos);

    void SetLightPos(const glm::vec3& pos);
    void SetLightAmbient(const glm::vec3& ambient);
    void SetLightDiffuse(const glm::vec3& diffuse);
    void SetLightSpecular(const glm::vec3& specular);

    void SetUseDiffuseTexture(bool hasTexture);
    void SetDiffuseSampler(GLint sampler);
    void SetUseSpecularTexture(bool hasTexture);
    void SetSpecularSampler(GLint sampler);
    void SetSpecularShininess(GLfloat shininess);

private:
    GLint m_locModel;
    GLint m_locView;
    GLint m_locProjection;
    // view position
    GLint m_locViewPos;
    // light
    GLint m_locLightPosition;
    GLint m_locLightAmbient;
    GLint m_locLightDiffuse;
    GLint m_locLightSpecular;
    // material
    GLint m_locHasDiffuseTexture;
    GLint m_locSamplerDiffuse;
    GLint m_locHasSpecularTexture;
    GLint m_locSamplerSpecular;
    GLint m_locShininess;
};

#endif // GLSLPROGRAMDIFFSPEC_H
