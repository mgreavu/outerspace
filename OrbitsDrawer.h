#ifndef ORBITSDRAWER_H
#define ORBITSDRAWER_H

#include <GL/glew.h>

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <vector>
#include <memory>

class GlslProgramBasic;

class OrbitsDrawer
{
public:
    OrbitsDrawer();
    ~OrbitsDrawer();

    bool Init();
    void Render(const glm::mat4& mvp);

private:

    struct Orbit {
        GLuint vbo;
        std::vector<glm::vec3> points;
    };

    std::unique_ptr<GlslProgramBasic> m_program;
    std::vector<Orbit> m_orbits;
};

#endif // ORBITSDRAWER_H
