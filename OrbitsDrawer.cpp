#include "OrbitsDrawer.h"
#include "JovianConstants.h"
#include "GlslProgramBasic.h"

#include <glm/trigonometric.hpp>

static constexpr unsigned int ORBIT_POINTS = 360;

static std::vector<glm::vec3> GenerateOrbit(float radius)
{
    std::vector<glm::vec3> points;
    points.reserve(ORBIT_POINTS);

    for (unsigned int i = 0; i < ORBIT_POINTS; ++i)
    {
        const float angle = glm::radians(static_cast<float>(i));
        points.emplace_back(radius * glm::cos(angle), 0, -radius * glm::sin(angle));
    }
    return points;
}

OrbitsDrawer::OrbitsDrawer()
{
}

OrbitsDrawer::~OrbitsDrawer()
{
    for (auto& orbit : m_orbits)
    {
        glDeleteBuffers(1, &orbit.vbo);
    }
}

bool OrbitsDrawer::Init()
{
    m_program = std::make_unique<GlslProgramBasic>();
    if (!m_program->Init())
    {
        return false;
    }

    m_orbits.resize(4);

    GLuint vbos[4];
    glGenBuffers(4, &vbos[0]);

    m_orbits[0].vbo = vbos[0];
    m_orbits[0].points = GenerateOrbit(ORBITAL_RADIUS_IO);

    m_orbits[1].vbo = vbos[1];
    m_orbits[1].points = GenerateOrbit(ORBITAL_RADIUS_EUROPA);

    m_orbits[2].vbo = vbos[2];
    m_orbits[2].points = GenerateOrbit(ORBITAL_RADIUS_GANYMEDE);

    m_orbits[3].vbo = vbos[3];
    m_orbits[3].points = GenerateOrbit(ORBITAL_RADIUS_CALLISTO);

    for (auto& orbit : m_orbits)
    {
        glBindBuffer(GL_ARRAY_BUFFER, orbit.vbo);
        glBufferData(GL_ARRAY_BUFFER,
                     static_cast<GLsizeiptr>(sizeof(glm::vec3) * orbit.points.size()),
                     reinterpret_cast<GLvoid*>(&orbit.points[0]),
                     GL_STATIC_DRAW);
    }

    m_program->Enable();
    m_program->SetLineColor({0.0f, 0.0f, 0.2f, 0.5f});
    return true;
}

void OrbitsDrawer::Render(const glm::mat4& mvp)
{
    m_program->Enable();
    m_program->SetMVP(mvp);

    glEnableVertexAttribArray(0);

    for (auto& orbit : m_orbits)
    {
        glBindBuffer(GL_ARRAY_BUFFER, orbit.vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), reinterpret_cast<GLvoid*>(0));

        glDrawArrays(GL_LINE_LOOP, 0, static_cast<GLsizei>(orbit.points.size()));
    }

    glDisableVertexAttribArray(0);
}
