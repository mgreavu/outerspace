#include "GlslProgramBasic.h"
#include <glm/gtc/type_ptr.hpp>

GlslProgramBasic::GlslProgramBasic()
    : m_locMVP(INVALID_UNIFORM_LOCATION)
    , m_locLineColor(INVALID_UNIFORM_LOCATION)
{
}

bool GlslProgramBasic::Init()
{
    bool ok = GlslProgram::Init();
    ok = ok && AddShader(GL_VERTEX_SHADER, "shaders/basic.vs");

    glBindAttribLocation(m_shaderProg, 0, "Position");

    ok = ok && AddShader(GL_FRAGMENT_SHADER, "shaders/basic.fs");
    ok = ok && Finalize();

    if (ok)
    {
        m_locMVP = GetUniformLocation("mvp");
        m_locLineColor = GetUniformLocation("lineColor");
        if (m_locMVP == static_cast<GLint>(INVALID_UNIFORM_LOCATION) ||
            m_locLineColor == static_cast<GLint>(INVALID_UNIFORM_LOCATION))
        {
            ok = false;
        }
    }
    return ok;
}

void GlslProgramBasic::SetMVP(const glm::mat4& mvp)
{
    glUniformMatrix4fv(m_locMVP, 1, GL_FALSE, glm::value_ptr(mvp));
}

void GlslProgramBasic::SetLineColor(const glm::vec4& color)
{
    glUniform4f(m_locLineColor, color.r, color.g, color.b, color.a);
}
