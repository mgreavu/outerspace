#pragma once
#include <graphics3d/GlslProgram.h>
#include "glm/mat4x4.hpp"


class GlslProgramBasic : public GlslProgram
{
public:
    GlslProgramBasic();
    ~GlslProgramBasic() override = default;

    bool Init() override;

    void SetMVP(const glm::mat4& mvp);
    void SetLineColor(const glm::vec4& color);

private:
    GLint m_locMVP;
    GLint m_locLineColor;
};
