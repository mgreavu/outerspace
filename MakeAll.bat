@echo on

setlocal

set BuildPlatform=%1
set BuildTarget=%2

if not defined BuildPlatform (
    set BuildPlatform=x64
)

if not defined BuildTarget (
    set BuildTarget=Debug
)

mkdir build
cd build

:: Run the cmake executable or the cmake-gui to configure the project
:: cmake -G "Visual Studio 16 2019" -A "%BuildPlatform%" ..
cmake -DCMAKE_INSTALL_PREFIX="d:/Install/OuterSpace" -G "Visual Studio 16 2019" -A "%BuildPlatform%" ..

:: ...and then build it with your chosen build tool
:: If using an IDE, simply build the INSTALL target. You can build the same install target from the command line like below.
:: This abstracts a native build tool’s command-line interface with a few options (see docs).
:: cmake --build . --target install --config Release
:: cmake --build . --target install --config Release --prefix "d:/Install/OuterSpace"
MSBuild OuterSpace.sln /p:Platform="%BuildPlatform%" /p:Configuration="%BuildTarget%"


:: Run the install step by using the install option of the cmake command (introduced in 3.15, older versions of CMake must use make install) from the command line.
:: This step will install the appropriate header files, libraries, and executables.
:: If using the cmake --install command, the installation prefix can be overridden via the --prefix argument
:: cmake --install . --config Release --prefix "d:/Install/graphics3d"

:: cd "%BuildTarget%"
:: dumpbin /symbols graphics3d.lib > graphics3d_symbols.txt

endlocal
