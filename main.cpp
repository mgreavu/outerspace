/*
*	Windows: Use SubSystem Console for Debug mode and SubSystem Windows for Release
*/
#if defined(_DEBUG)
#define SDL_MAIN_HANDLED
#endif
#include <SDL2/SDL.h>
#include <SDL2/SDL_log.h>
#include <GL/glew.h>
#include <FreeImage.h>

#include <graphics3d/ProjectorFPS.h>
#include <graphics3d/SceneGraph.h>
#include <graphics3d/Model3d.h>
#include <graphics3d/JsonParser.h>
#include "JovianSystemController.h"
#include "SGRendererGL.h"

static constexpr float MOVEMENT_LARGE = 50.0f;
static constexpr float MOVEMENT_SMALL = 1.0f;

static int resizeCallback(void* data, SDL_Event* event) // https://stackoverflow.com/questions/32294913/getting-contiunous-window-resize-event-in-sdl-2
{
	if (event->type == SDL_WINDOWEVENT && event->window.event == SDL_WINDOWEVENT_RESIZED)
	{
		SDL_Window* win = SDL_GetWindowFromID(event->window.windowID);
		if (win == (SDL_Window*)data)
		{
			SDL_Log("resizing.....\n");
		}
	}
	return 0;
}

int main(int argc, char* argv[])
{
	FreeImage_Initialise(false);

	if (SDL_Init(SDL_INIT_VIDEO))
	{
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "Failed to init SDL Video, error: %s\n", SDL_GetError());
		return -1;
	}

	enum class SCREENSIZE
	{
		qHD,			//  960 x  540
		HD,				// 1280 x  720
		FHD,			// 1920 x 1080
		QHD,			// 2560 x 1440
		FULLSCREEN
	};

	SCREENSIZE curr_screen_size = SCREENSIZE::HD;
	SCREENSIZE last_non_fullscreen_size = SCREENSIZE::HD;

	int win_width  = 1280;
	int win_height = 720;

	unsigned int window_flags = SDL_WINDOW_OPENGL;
	SDL_Window* sdlWindow = SDL_CreateWindow(
		"Jovian System", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, win_width, win_height, window_flags);
	if (sdlWindow == nullptr)
	{
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "Could not create window: %s\n", SDL_GetError());
		return -1;
	}

	SDL_AddEventWatch(resizeCallback, sdlWindow);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GLContext sdlContext = SDL_GL_CreateContext(sdlWindow);
	if (sdlContext == NULL)
	{
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
		SDL_DestroyWindow(sdlWindow);
		SDL_Quit();
		return -1;
	}

	if (SDL_GL_SetSwapInterval(1)) // Use VSYNC
	{
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "Failed to set the swap interval: %s\n", SDL_GetError());
	}

	// Initialize GL Extension Wrangler (GLEW)
	glewExperimental = GL_TRUE; // defined by GLEW, expose OpenGL 3.x+ interfaces
	GLenum err = glewInit();
	if (err != GLEW_OK)
	{
		SDL_LogError(SDL_LOG_CATEGORY_VIDEO, "Failed to init GLEW\n");
		SDL_GL_DeleteContext(sdlContext);
		SDL_DestroyWindow(sdlWindow);
		SDL_Quit();
		return -1;
	}

	std::shared_ptr<Projector> m_camera = std::make_shared<ProjectorFPS>(45.0f, 0.2f, 5000.0f);
	m_camera->SetPosition(glm::vec3(0.0f, 50.0f, 550.0f));
	m_camera->Update();

	std::unique_ptr<SGRendererGL> m_renderer = std::make_unique<SGRendererGL>();
	m_renderer->Init(m_camera);
	m_renderer->SetCullMode(CullMode::CULL_BACK);
	m_renderer->Resize(win_width, win_height);
	m_camera->Update();

	std::shared_ptr<SceneGraph> m_sceneGraph = std::make_shared<SceneGraph>();
	JsonParser jsonParser(m_sceneGraph->GetRoot());
	if (jsonParser.Load("jovian_system.json"))
	{
		m_renderer->SetScene(m_sceneGraph);
	}

	std::unique_ptr<sim_control::JovianSystemController> m_jsc = std::make_unique<sim_control::JovianSystemController>(m_sceneGraph);
	m_jsc->SetSimulationSpeed(500.0f);

	glm::vec2 posStart;
	float headingStart = m_camera->GetHeading();
	float tiltingStart = m_camera->GetTilting();

	bool isRunning = true;
	SDL_Event sdl_event;
	while (isRunning)
	{
		while (SDL_PollEvent(&sdl_event) != 0)
		{
			if (sdl_event.type == SDL_QUIT)
			{
				isRunning = false;
			}
			else if (sdl_event.type == SDL_MOUSEWHEEL)
			{
				(sdl_event.wheel.y > 0) ? m_camera->MoveForward(5.0f) : m_camera->MoveBackward(5.0f);
				m_camera->Update();
			}
			else if (sdl_event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (sdl_event.button.button == SDL_BUTTON_LEFT)
				{
					posStart = glm::vec2(sdl_event.button.x, sdl_event.button.y);
					headingStart = m_camera->GetHeading();
					tiltingStart = m_camera->GetTilting();

					if (m_sceneGraph)
					{
						const glm::vec3 rayDir = m_camera->Raycast(glm::i32vec2(sdl_event.button.x, sdl_event.button.y));
						const glm::vec3& rayOrigin = m_camera->GetCameraPosition();
						float distNearest = std::numeric_limits<float>::max();
						SceneNode* pNearest = nullptr;
						m_sceneGraph->Select(rayOrigin, rayDir, distNearest, &pNearest);
						if (pNearest)
						{
							SDL_Log("Nearest found at dist: %.4f\n", static_cast<double>(distNearest));

							char textline[128];
							memset(textline, 0, 128);
							snprintf(textline, 128, "Selected: %s", pNearest->GetModel()->GetName().c_str());
							m_renderer->InsertText(1, textline);

							memset(textline, 0, 128);
							snprintf(textline, 128, "Distance: %.2f (%.2f km)", distNearest, distNearest * 1000.0f);
							m_renderer->InsertText(2, textline);
						}
					}
				}
			}
			else if (sdl_event.type == SDL_MOUSEMOTION)
			{
				if (sdl_event.button.button == SDL_BUTTON_LEFT)
				{
					glm::vec2 posCurrent = glm::vec2(sdl_event.button.x, sdl_event.button.y);
					glm::vec2 mouseDelta = posCurrent - posStart;

					const glm::i32vec2& size = m_camera->GetViewport();

					m_camera->SetHeading(headingStart + 360.0f * (mouseDelta.x / size.x));
					m_camera->SetTilting(tiltingStart + 90.0f * (mouseDelta.y / size.y));
					m_camera->Update();
				}
			}
			else if (sdl_event.type == SDL_KEYDOWN)
			{
				switch (sdl_event.key.keysym.sym)
				{
				case SDLK_ESCAPE:
				{
					isRunning = false;
					break;
				}
				case SDLK_F11:
				{
					if (curr_screen_size != SCREENSIZE::FULLSCREEN) // then set it to fullscreen and save prev state
					{
						last_non_fullscreen_size = curr_screen_size;
						curr_screen_size = SCREENSIZE::FULLSCREEN;
						SDL_SetWindowFullscreen(sdlWindow, window_flags | SDL_WINDOW_FULLSCREEN_DESKTOP);
					}
					else // is currently fullscreen, set it back to the prev state
					{
						curr_screen_size = last_non_fullscreen_size;
						SDL_SetWindowFullscreen(sdlWindow, window_flags);
					}
					SDL_GetWindowSize(sdlWindow, &win_width, &win_height);
					m_renderer->Resize(win_width, win_height);
					m_camera->Update();
					break;
				}
				case SDLK_F10: // toggle screensizes, does nothing if fullscreen
				{
					switch (curr_screen_size)
					{
						case SCREENSIZE::FULLSCREEN:
						{
							break;
						}
						case SCREENSIZE::qHD:
						{
							curr_screen_size = SCREENSIZE::HD;
							SDL_SetWindowSize(sdlWindow, 1280, 720);
							SDL_GetWindowSize(sdlWindow, &win_width, &win_height);
							m_renderer->Resize(win_width, win_height);
							m_camera->Update();
							break;
						}
						case SCREENSIZE::HD:
						{
							curr_screen_size = SCREENSIZE::FHD;
							SDL_SetWindowSize(sdlWindow, 1920, 1080);
							SDL_GetWindowSize(sdlWindow, &win_width, &win_height);
							m_renderer->Resize(win_width, win_height);
							m_camera->Update();
							break;
						}
						case SCREENSIZE::FHD:
						{
							curr_screen_size = SCREENSIZE::QHD;
							SDL_SetWindowSize(sdlWindow, 2560, 1440);
							SDL_GetWindowSize(sdlWindow, &win_width, &win_height);
							m_renderer->Resize(win_width, win_height);
							m_camera->Update();
							break;
						}
						case SCREENSIZE::QHD:
						{
							curr_screen_size = SCREENSIZE::qHD;
							SDL_SetWindowSize(sdlWindow, 960, 540);
							SDL_GetWindowSize(sdlWindow, &win_width, &win_height);
							m_renderer->Resize(win_width, win_height);
							m_camera->Update();
							break;
						}
					}
					break;
				}
				case SDLK_UP:
				{
					m_camera->MoveBackward(sdl_event.key.keysym.mod & KMOD_LSHIFT ? MOVEMENT_SMALL : MOVEMENT_LARGE);
					m_camera->Update();
					break;
				}
				case SDLK_DOWN:
				{
					m_camera->MoveForward(sdl_event.key.keysym.mod & KMOD_LSHIFT ? MOVEMENT_SMALL : MOVEMENT_LARGE);
					m_camera->Update();
					break;
				}
				case SDLK_LEFT:
				{
					m_camera->StrafeLeft(sdl_event.key.keysym.mod & KMOD_LSHIFT ? MOVEMENT_SMALL : MOVEMENT_LARGE);
					m_camera->Update();
					break;
				}
				case SDLK_RIGHT:
				{
					m_camera->StrafeRight(sdl_event.key.keysym.mod & KMOD_LSHIFT ? MOVEMENT_SMALL : MOVEMENT_LARGE);
					m_camera->Update();
					break;
				}
				case SDLK_PAGEUP:
				{
					m_camera->MoveUp(sdl_event.key.keysym.mod & KMOD_LSHIFT ? MOVEMENT_SMALL : MOVEMENT_LARGE);
					m_camera->Update();
					break;
				}
				case SDLK_PAGEDOWN:
				{
					m_camera->MoveDown(sdl_event.key.keysym.mod & KMOD_LSHIFT ? MOVEMENT_SMALL : MOVEMENT_LARGE);
					m_camera->Update();
					break;
				}
				case SDLK_a:
				{
					float angle = m_camera->GetHeading();
					angle += 2.0f;
					m_camera->SetHeading(angle);
					m_camera->Update();
					break;
				}
				case SDLK_d:
				{
					float angle = m_camera->GetHeading();
					angle -= 2.0f;
					m_camera->SetHeading(angle);
					m_camera->Update();
					break;
				}
				case SDLK_w:
				{
					float angle = m_camera->GetTilting();
					angle += 2.0f;
					m_camera->SetTilting(angle);
					m_camera->Update();
					break;
				}
				case SDLK_s:
				{
					float angle = m_camera->GetTilting();
					angle -= 2.0f;
					m_camera->SetTilting(angle);
					m_camera->Update();
					break;
				}
				case SDLK_b:
				{
					const RenderingFlags flags = m_renderer->GetFlags();
					if (flags & RenderingFlags::FLAG_DRAW_BBOX)
					{
						m_renderer->RemoveFlag(RenderingFlags::FLAG_DRAW_BBOX);
					}
					else
					{
						m_renderer->AddFlag(RenderingFlags::FLAG_DRAW_BBOX);
					}
					break;
				}
				case SDLK_f:
				{
					const RenderingFlags flags = m_renderer->GetFlags();
					if (flags & RenderingFlags::FLAG_DRAW_WIREFRAME)
					{
						m_renderer->RemoveFlag(RenderingFlags::FLAG_DRAW_WIREFRAME);
					}
					else
					{
						m_renderer->AddFlag(RenderingFlags::FLAG_DRAW_WIREFRAME);
					}
					break;
				}
				default: break;
				}
			}
		} // poll until all events are handled

		// update game state, draw the current frame
		m_jsc->Update();
		m_renderer->Render();

		// swap to new updated screen to render
		SDL_GL_SwapWindow(sdlWindow); // to investigate: the very first call returns GL error 1281
	}

	// clean up
	m_renderer.reset();
	SDL_GL_DeleteContext(sdlContext);
	SDL_DestroyWindow(sdlWindow);
	SDL_Quit();
	FreeImage_DeInitialise();
	return 0;
}
