#include "SGRendererGL.h"
#include "OrbitsDrawer.h"
#include "GlslProgramBasic.h"
#include <graphics3d/Projector.h>
#include <graphics3d/SceneNode.h>
#include <graphics3d/SceneGraph.h>
#include <graphics3d/Model3d.h>
#include <graphics3d/Material.h>
#include <graphics3d/Texture2D.h>
#include <graphics3d/SkyBox.h>
#include <graphics3d/TextBillboard.h>

#include <SDL2/SDL_stdinc.h>
#include <SDL2/SDL_log.h>
#include <glm/gtc/matrix_transform.hpp>

#ifdef DEVELOPER_DEBUG
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#endif

static constexpr unsigned int MAX_BBOX_COUNT = 128;

SGRendererGL::SGRendererGL()
    : m_flags(RenderingFlags::FLAG_ENBLE_TEXTURES)
    , m_vao(0)
    , m_skyboxDrawer(nullptr)
    , m_infoPanelDrawer(nullptr)
    , m_orbitsDrawer(nullptr)
    , m_bboxBuffer(0)
    , m_bboxBufferOffset(0)
{
}

SGRendererGL::~SGRendererGL()
{
    CleanUp();
}

bool SGRendererGL::Init(const std::shared_ptr<Projector>& proj)
{
    m_pProj = proj;

    if (!m_progDiffSpec.Init())
    {
        return false;
    }

    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    m_skyboxDrawer = std::make_unique<SkyBox>("png");
    m_skyboxDrawer->Load("data/cubemaps/space/stars/");

    m_infoPanelDrawer = std::make_unique<TextBillboard>();
    m_infoPanelDrawer->Load();

    m_orbitsDrawer = std::make_unique<OrbitsDrawer>();
    m_orbitsDrawer->Init();

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.0f, 0.0f, 0.1f, 1.0f);

    m_progDiffSpec.Enable();
    m_progDiffSpec.SetLightPos({-500.0f, 500.0f, 500.0f});
    m_progDiffSpec.SetLightAmbient({0.2f, 0.2f, 0.2f});
    m_progDiffSpec.SetLightDiffuse({0.5f, 0.5f, 0.5f});
    m_progDiffSpec.SetLightSpecular({1.0f, 1.0f, 1.0f});
    m_progDiffSpec.SetDiffuseSampler(0);
    m_progDiffSpec.SetSpecularSampler(1);
    return true;
}

void SGRendererGL::CleanUp()
{
    RemoveScene();
    m_orbitsDrawer.reset();
    m_infoPanelDrawer.reset();
    m_skyboxDrawer.reset();
    glDeleteVertexArrays(1, &m_vao);
}

void SGRendererGL::Resize(int cx, int cy)
{
    m_pProj->SetViewport(glm::i32vec2(cx, cy));
    m_infoPanelDrawer->SetViewport(glm::i32vec2(cx, cy));
    glViewport(0, 0, cx, cy); // x, y: left bottom corner; cx, cy: width, height
}

void SGRendererGL::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (m_orbitsDrawer)
    {
        m_orbitsDrawer->Render(m_pProj->GetCombinedMVPMatrix());
    }

    if (m_pScene)
    {
        glEnableVertexAttribArray(0); // vertices
        glEnableVertexAttribArray(2); // normals

        m_bboxBufferOffset = 0;
        RenderScene(m_pScene->GetRoot());

        glDisableVertexAttribArray(2); // normals
        glDisableVertexAttribArray(0); // vertices
    }

    if (m_skyboxDrawer)
    {
        // strip the MVP matrix of the translation, draw centered at origin
        glm::mat4 matMVP = glm::mat4(1.0f);
        matMVP = m_pProj->GetCombinedMVPMatrix() * glm::translate(matMVP, m_pProj->GetCameraPosition());
        m_skyboxDrawer->Render(matMVP);
    }

    if (m_infoPanelDrawer)
    {
        m_infoPanelDrawer->Render(m_pProj->GetViewMatrix());
    }
}

void SGRendererGL::RenderScene(SceneNode* pScene)
{
    const Model3d* pModel = pScene->GetModel();
    if (pModel)
    {
        const glm::mat4& matWorld = pScene->GetWorldTransform();
        const glm::mat4 matMVP = m_pProj->GetCombinedMVPMatrix() * matWorld;

#ifdef DEVELOPER_DEBUG
        std::cout << "mvp: " << glm::to_string(matMVP) << std::endl;
#endif

        m_ffCuller.Load(matMVP);

        m_progDiffSpec.Enable();
        m_progDiffSpec.SetModel(matWorld);
        m_progDiffSpec.SetView(m_pProj->GetViewMatrix());
        m_progDiffSpec.SetProjection(m_pProj->GetProjectionMatrix());
        m_progDiffSpec.SetViewPos(m_pProj->GetCameraPosition());

        for (Mesh::const_iterator cmit = pModel->cbegin(); cmit != pModel->cend(); ++cmit)
        {
            // frustum culling
            if (m_ffCuller.RectPrismCulled((*cmit).m_bbox) == FFC_CULLED)
            {
#ifdef DEVELOPER_DEBUG
                SDL_Log("Culled submesh in model '%s'\n", pModel->GetName().c_str());
#endif
                continue;
            }

            const Material* pMaterial = (*cmit).m_pMaterial.get();
            assert(pMaterial);

            m_progDiffSpec.SetUseDiffuseTexture(pMaterial->m_pTexDiffuse ? true : false);
            m_progDiffSpec.SetUseSpecularTexture(pMaterial->m_pTexSpecular ? true : false);
            m_progDiffSpec.SetSpecularShininess(pMaterial->m_shininess);

            glBindBuffer(GL_ARRAY_BUFFER, (*cmit).m_VB);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(0)); // vertices
            glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(sizeof(glm::vec3) + sizeof(glm::vec2))); // normals

            if (m_flags & RenderingFlags::FLAG_ENBLE_TEXTURES)
            {
                if (pMaterial->m_pTexDiffuse || pMaterial->m_pTexSpecular)
                {
                    glEnableVertexAttribArray(1);
                    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(sizeof(glm::vec3))); // texture coords
                }

                if (pMaterial->m_pTexDiffuse)
                {
                    if (pMaterial->m_pTexDiffuse->HasAlpha())
                    {
                        glEnable(GL_BLEND);
                        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                    }

                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, pMaterial->m_pTexDiffuse->GetTexObj());
                }

                if (pMaterial->m_pTexSpecular)
                {
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, pMaterial->m_pTexSpecular->GetTexObj());
                }
            }

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*cmit).m_IB); // indices

            glDrawElements(GL_TRIANGLES, static_cast<GLsizei>((*cmit).m_indices.size()), GL_UNSIGNED_INT, nullptr);

            if (m_flags & RenderingFlags::FLAG_ENBLE_TEXTURES)
            {
                if (pMaterial->m_pTexDiffuse || pMaterial->m_pTexSpecular)
                {
                    glDisableVertexAttribArray(1); // texture coords
                }

                if (pMaterial->m_pTexDiffuse)
                {
                    if (pMaterial->m_pTexDiffuse->HasAlpha())
                    {
                        glDisable(GL_BLEND);
                    }
                }
            }
        } // end of meshes loop

        if (m_flags & RenderingFlags::FLAG_DRAW_BBOX)
        {
            _ASSERT(m_bboxBufferOffset <= static_cast<GLint>(m_bboxVertices.size() - 16));

            m_progBBox->Enable();
            m_progBBox->SetMVP(matMVP);

            glBindBuffer(GL_ARRAY_BUFFER, m_bboxBuffer);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(0)); // vertices

            glDrawArrays(GL_LINE_LOOP, m_bboxBufferOffset, 4);
            glDrawArrays(GL_LINE_LOOP, m_bboxBufferOffset + 4, 4);
            glDrawArrays(GL_LINE_LOOP, m_bboxBufferOffset + 8, 4);
            glDrawArrays(GL_LINE_LOOP, m_bboxBufferOffset + 12, 4);

            m_bboxBufferOffset += 16;
        }
    }

    for (SceneNode::const_iterator cit = pScene->cbegin(); cit != pScene->cend(); ++cit)
    {
        RenderScene((*cit).get());
    }
}

void SGRendererGL::InsertText(int lineNo, const std::string &text)
{
    m_infoPanelDrawer->InsertLine(lineNo, text);
}

void SGRendererGL::SetScene(const std::shared_ptr<SceneGraph> &pScene)
{
    m_pScene = pScene;

    if (m_pScene)
    {
        PrepareScene(m_pScene->GetRoot());

        if (m_flags & RenderingFlags::FLAG_DRAW_BBOX)
        {
            m_bboxVertices.reserve(MAX_BBOX_COUNT * 16);
            PrepareBoundingBoxes(m_pScene->GetRoot());
            SDL_Log("m_bboxVertices.size() = %" SDL_PRIu64 "\n", m_bboxVertices.size());

            glGenBuffers(1, &m_bboxBuffer); // bbox vertices
            glBindBuffer(GL_ARRAY_BUFFER, m_bboxBuffer);
            glBufferData(GL_ARRAY_BUFFER,
                         static_cast<GLsizeiptr>(sizeof(Vertex) * m_bboxVertices.size()),
                         reinterpret_cast<GLvoid*>(&m_bboxVertices[0]),
                         GL_STATIC_DRAW);
        }
    }
}

void SGRendererGL::RemoveScene()
{
    if (m_pScene)
    {
        RemoveScene(m_pScene->GetRoot());
        m_pScene.reset();

        if (m_flags & RenderingFlags::FLAG_DRAW_BBOX)
        {
            m_progBBox.reset();
            glDeleteBuffers(1, &m_bboxBuffer);
            m_bboxBuffer = 0;
            m_bboxVertices.clear();
            m_bboxVertices.shrink_to_fit();
        }
    }
}

void SGRendererGL::PrepareScene(SceneNode* pScene)
{
    Model3d* pModel = pScene->GetModel();
    if (pModel)
    {
        SDL_Log("PrepareScene() --> model %s\n", pModel->GetName().c_str());
        for (Mesh::iterator mit = pModel->begin(); mit != pModel->end(); ++mit)
        {
            glGenBuffers(1, &(*mit).m_VB); // vertices
            glBindBuffer(GL_ARRAY_BUFFER, (*mit).m_VB);
            glBufferData(GL_ARRAY_BUFFER,
                         static_cast<GLsizeiptr>(sizeof(Vertex) * (*mit).m_vertices.size()),
                         reinterpret_cast<GLvoid*>(&(*mit).m_vertices[0]),
                         GL_STATIC_DRAW);

            glGenBuffers(1, &(*mit).m_IB); // indices
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (*mit).m_IB);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                         static_cast<GLsizeiptr>(sizeof(unsigned int) * (*mit).m_indices.size()),
                         reinterpret_cast<GLvoid*>(&(*mit).m_indices[0]),
                         GL_STATIC_DRAW);

            assert((*mit).m_pMaterial);

            std::shared_ptr<Texture2D> spTexture = (*mit).m_pMaterial->m_pTexDiffuse;
            if (spTexture && !spTexture->IsReady())
            {
                GLuint texObj;
                glGenTextures(1, &texObj);
                spTexture->SetTexObj(texObj);

                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, texObj);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                unsigned int cols = 0, rows = 0;
                const void* pPixels = spTexture->GetData(cols, rows);

                // all of the mutable storage calls are capable of both allocating memory and transferring pixel data into that memory
                // mutable storage creation also uploads data
                // https://www.khronos.org/opengl/wiki/Texture_Storage
                const GLint internalformat = (spTexture->GetBpp() == 32) ? GL_RGBA : GL_RGB;
                const GLenum format = (spTexture->GetBpp() == 32) ? GL_RGBA : GL_RGB;
                glTexImage2D(GL_TEXTURE_2D, 0, internalformat, static_cast<GLsizei>(cols), static_cast<GLsizei>(rows), 0, format, GL_UNSIGNED_BYTE, pPixels);
                spTexture->FreeData();

                spTexture->SetReady(true);
            }

            spTexture = (*mit).m_pMaterial->m_pTexSpecular;
            if (spTexture && !spTexture->IsReady())
            {
                GLuint texObj;
                glGenTextures(1, &texObj);
                spTexture->SetTexObj(texObj);

                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, texObj);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

                unsigned int cols = 0, rows = 0;
                const void* pPixels = spTexture->GetData(cols, rows);

                // all of the mutable storage calls are capable of both allocating memory and transferring pixel data into that memory
                // mutable storage creation also uploads data
                // https://www.khronos.org/opengl/wiki/Texture_Storage
                const GLint internalformat = (spTexture->GetBpp() == 32) ? GL_RGBA : GL_RGB;
                const GLenum format = (spTexture->GetBpp() == 32) ? GL_RGBA : GL_RGB;
                glTexImage2D(GL_TEXTURE_2D, 0, internalformat, static_cast<GLsizei>(cols), static_cast<GLsizei>(rows), 0, format, GL_UNSIGNED_BYTE, pPixels);
                spTexture->FreeData();

                spTexture->SetReady(true);
            }
        }
    }

    for (SceneNode::const_iterator it = pScene->cbegin(); it != pScene->cend(); ++it)
    {
        PrepareScene((*it).get());
    }
}

void SGRendererGL::PrepareBoundingBoxes(SceneNode* pScene)
{
    const Model3d* pModel = pScene->GetModel();
    if (pModel)
    {
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(0));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(4));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(6));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(2));

        m_bboxVertices.emplace_back(pModel->GetBBox().corner(1));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(5));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(7));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(3));

        m_bboxVertices.emplace_back(pModel->GetBBox().corner(2));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(6));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(7));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(3));

        m_bboxVertices.emplace_back(pModel->GetBBox().corner(0));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(4));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(5));
        m_bboxVertices.emplace_back(pModel->GetBBox().corner(1));
    }

    for (SceneNode::const_iterator cit = pScene->cbegin(); cit != pScene->cend(); ++cit)
    {
        PrepareBoundingBoxes((*cit).get());
    }
}

void SGRendererGL::RemoveScene(SceneNode* pScene)
{
    const Model3d* pModel = pScene->GetModel();
    if (pModel)
    {
        for (Mesh::const_iterator cmit = pModel->cbegin(); cmit != pModel->cend(); ++cmit)
        {
            std::shared_ptr<Texture2D> spTexture = (*cmit).m_pMaterial->m_pTexDiffuse;
            if (spTexture && spTexture->IsReady())
            {
                const GLuint texObj = spTexture->GetTexObj();
                glDeleteTextures(1, &texObj);
                spTexture->SetReady(false);
            }
            spTexture = (*cmit).m_pMaterial->m_pTexSpecular;
            if (spTexture && spTexture->IsReady())
            {
                const GLuint texObj = spTexture->GetTexObj();
                glDeleteTextures(1, &texObj);
                spTexture->SetReady(false);
            }
            glDeleteBuffers(1, &(*cmit).m_IB);
            glDeleteBuffers(1, &(*cmit).m_VB);
        }
    }

    for (SceneNode::const_iterator it = pScene->cbegin(); it != pScene->cend(); ++it)
    {
        RemoveScene((*it).get());
    }
}

void SGRendererGL::AddFlag(RenderingFlags flag)
{
    if (m_flags & flag)
    {
        return;
    }
    if (flag == RenderingFlags::FLAG_DRAW_WIREFRAME)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    else if (flag == RenderingFlags::FLAG_DRAW_BBOX)
    {
        m_progBBox = std::make_unique<GlslProgramBasic>();
        if (m_progBBox->Init())
        {
            m_progBBox->Enable();
            m_progBBox->SetLineColor({1.0f, 0.0f, 0.0f, 1.0f});

            m_bboxVertices.reserve(MAX_BBOX_COUNT * 16);
            PrepareBoundingBoxes(m_pScene->GetRoot());
            SDL_Log("m_bboxVertices.size() = %" SDL_PRIu64 "\n", m_bboxVertices.size());

            glGenBuffers(1, &m_bboxBuffer); // bbox vertices
            glBindBuffer(GL_ARRAY_BUFFER, m_bboxBuffer);
            glBufferData(GL_ARRAY_BUFFER,
                         static_cast<GLsizeiptr>(sizeof(Vertex) * m_bboxVertices.size()),
                         reinterpret_cast<GLvoid*>(&m_bboxVertices[0]),
                         GL_STATIC_DRAW);
        }
    }
    m_flags |= flag;
}

void SGRendererGL::RemoveFlag(RenderingFlags flag)
{
    if (!(m_flags & flag))
    {
        return;
    }
    if (flag == RenderingFlags::FLAG_DRAW_WIREFRAME)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    else if (flag == RenderingFlags::FLAG_DRAW_BBOX)
    {
        m_progBBox.reset();
        glDeleteBuffers(1, &m_bboxBuffer);
        m_bboxBuffer = 0;
        m_bboxVertices.clear();
        m_bboxVertices.shrink_to_fit();
    }
    m_flags &= ~flag;
}

RenderingFlags SGRendererGL::GetFlags() const
{
    return static_cast<RenderingFlags>(m_flags);
}

void SGRendererGL::SetCullMode(CullMode mode)
{
    switch(mode)
    {
    case CullMode::CULL_NONE:
        {
            glDisable(GL_CULL_FACE);
            break;
        }
    case CullMode::CULL_BACK:
        {
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);
            break;
        }
    case CullMode::CULL_FRONT:
        {
            glCullFace(GL_FRONT);
            glEnable(GL_CULL_FACE);
            break;
        }
    }
}
