#pragma once
#include "GlslProgramDiffSpec.h"
#include <graphics3d/RenderFlags.h>
#include <graphics3d/FastFrustumCulling.h>
#include <graphics3d/Vertex.h>

#include <GL/glew.h>

#include <vector>
#include <memory>

class Projector;
class SceneNode;
class SceneGraph;
class SkyBox;
class TextBillboard;
class OrbitsDrawer;
class GlslProgramBasic;

class SGRendererGL
{
public:
    SGRendererGL();
    ~SGRendererGL();

    bool Init(const std::shared_ptr<Projector>& proj);
    void AddFlag(RenderingFlags flag);
    void RemoveFlag(RenderingFlags flag);
    RenderingFlags GetFlags() const;
    void SetCullMode(CullMode mode);
    void SetScene(const std::shared_ptr<SceneGraph>& pScene);
    void InsertText(int lineNo, const std::string& text);
    void Render();
    void RemoveScene();
    void Resize(int cx, int cy);

private:
    void PrepareScene(SceneNode* pScene);
    void RenderScene(SceneNode* pScene);
    void RemoveScene(SceneNode* pScene);
    void PrepareBoundingBoxes(SceneNode* pScene);
    void CleanUp();

    std::shared_ptr<Projector> m_pProj;
    int m_flags;
    GLuint m_vao;

    std::shared_ptr<SceneGraph> m_pScene;
    FastFrustumCulling m_ffCuller;

    // light
    GlslProgramDiffSpec m_progDiffSpec;

    std::unique_ptr<SkyBox> m_skyboxDrawer;
    std::unique_ptr<TextBillboard> m_infoPanelDrawer;
    std::unique_ptr<OrbitsDrawer> m_orbitsDrawer;

    // bounding boxes
    std::unique_ptr<GlslProgramBasic> m_progBBox;
    GLuint m_bboxBuffer;
    std::vector<Vertex> m_bboxVertices;
    GLint m_bboxBufferOffset;
};

