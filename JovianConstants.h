#ifndef JOVIANCONSTANTS_H
#define JOVIANCONSTANTS_H

#include <string>

extern const std::string JUPITER;
extern const std::string IO;
extern const std::string EUROPA;
extern const std::string GANYMEDE;
extern const std::string CALLISTO;

extern const float DISTANCE_FACTOR;

// in thousands of kilometers
extern const float RADIUS_JUPITER;
extern const float RADIUS_IO;
extern const float RADIUS_EUROPA;
extern const float RADIUS_GANYMEDE;
extern const float RADIUS_CALLISTO;

extern const float ORBITAL_RADIUS_IO;
extern const float ORBITAL_RADIUS_EUROPA;
extern const float ORBITAL_RADIUS_GANYMEDE;
extern const float ORBITAL_RADIUS_CALLISTO;

// in kilometers per second
extern const float ORBITAL_SPEED_IO;
extern const float ORBITAL_SPEED_EUROPA;
extern const float ORBITAL_SPEED_GANYMEDE;
extern const float ORBITAL_SPEED_CALLISTO;

// in kilometers per second
extern const float ROTATION_SPEED_JUPITER;
extern const float ROTATION_SPEED_IO;
extern const float ROTATION_SPEED_EUROPA;
extern const float ROTATION_SPEED_GANYMEDE;
extern const float ROTATION_SPEED_CALLISTO;

#endif // JOVIANCONSTANTS_H
