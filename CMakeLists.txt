﻿# CMakeList.txt : CMake project for OuterSpace, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.20)

set(MY_PACKAGES_FOLDER "d:/Install")

set(VCPKG_PACKAGES_FOLDER "d:/vcpkg/installed/x64-windows")

set(PKG_CONFIG_EXECUTABLE "d:/vcpkg/downloads/tools/msys2/9a1ec3f33446b195/mingw32/bin/pkg-config.exe")

list(APPEND CMAKE_PREFIX_PATH "${VCPKG_PACKAGES_FOLDER}")

list(APPEND CMAKE_MODULE_PATH "${VCPKG_PACKAGES_FOLDER}")
list(APPEND CMAKE_MODULE_PATH "${VCPKG_PACKAGES_FOLDER}/share/jxr")
list(APPEND CMAKE_MODULE_PATH "${VCPKG_PACKAGES_FOLDER}/share/libraw")

list(APPEND CMAKE_PREFIX_PATH "${MY_PACKAGES_FOLDER}/graphics3d")


project("OuterSpace")

find_package(graphics3d CONFIG REQUIRED)
find_package(SDL2 CONFIG REQUIRED)

add_executable(${PROJECT_NAME})

set_target_properties(${PROJECT_NAME} PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)

set_property(TARGET ${PROJECT_NAME} PROPERTY VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}/build/Debug")

target_sources(${PROJECT_NAME}
	PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/GlslProgramBasic.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/GlslProgramDiffSpec.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/JovianSystemController.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/OrbitsDrawer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/SGRendererGL.cpp
)

target_include_directories(${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}
)

target_link_libraries(${PROJECT_NAME}
	PRIVATE
		graphics3d::graphics3d
        SDL2::SDL2
        SDL2::SDL2main
)

# set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MD")
# set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MDd")
