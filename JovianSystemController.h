#ifndef JOVIANBODIESCONTROLLER_H
#define JOVIANBODIESCONTROLLER_H

#include <graphics3d/Stopwatch.h>
#include <memory>

class SceneGraph;

namespace sim_control {

class JovianSystemController
{
public:
    JovianSystemController(const std::shared_ptr<SceneGraph>& sceneGraph);

    void SetSimulationSpeed(float factor);
    inline float GetSimulationSpeed() const { return m_simulationSpeed; }

    void Update();

private:
    std::shared_ptr<SceneGraph> m_sceneGraph;
    graphics_engine::Stopwatch m_timer;
    float m_simulationSpeed; // simulation speed factor

    // orbital angles
    float m_angleIo; // radians
    float m_angleEuropa; // radians
    float m_angleGanymede; // radians
    float m_angleCallisto; // radians

    // rotation angles
    float m_angleRotJupiter; // radians
    float m_angleRotIo; // radians
    float m_angleRotEuropa; // radians
    float m_angleRotGanymede; // radians
    float m_angleRotCallisto; // radians
};

} // namespace sim_control
#endif // JOVIANBODIESCONTROLLER_H
