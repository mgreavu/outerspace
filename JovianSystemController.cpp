#include "JovianSystemController.h"
#include "JovianConstants.h"
#include <graphics3d/SceneGraph.h>
#include <glm/trigonometric.hpp>

static constexpr glm::vec3 YAXIS(0.0f, 1.0f, 0.0f);

const float DISTANCE_FACTOR = 1000.0f;

const std::string JUPITER { "Jupiter" };
const std::string IO { "Io" };
const std::string EUROPA { "Europa" };
const std::string GANYMEDE { "Ganymede" };
const std::string CALLISTO { "Callisto" };

// in thousands of kilometers
const float RADIUS_JUPITER = 69.911f;
const float RADIUS_IO = 1.815f;
const float RADIUS_EUROPA = 1.569f;
const float RADIUS_GANYMEDE = 2.63f;
const float RADIUS_CALLISTO = 2.4f;

const float ORBITAL_RADIUS_IO = 421.6f;
const float ORBITAL_RADIUS_EUROPA = 670.9f;
const float ORBITAL_RADIUS_GANYMEDE = 1070.4f;
const float ORBITAL_RADIUS_CALLISTO = 1883.f;

// in kilometers per second
const float ORBITAL_SPEED_IO = 17.334f;
const float ORBITAL_SPEED_EUROPA = 13.743f;
const float ORBITAL_SPEED_GANYMEDE = 10.88f;
const float ORBITAL_SPEED_CALLISTO = 8.204f;

// in kilometers per second
const float ROTATION_SPEED_JUPITER = 12.6619f;
const float ROTATION_SPEED_IO = 0.07527f;         // synchronous rotation, tidally locked
const float ROTATION_SPEED_EUROPA = 0.03214f;
const float ROTATION_SPEED_GANYMEDE = 0.01607f;
const float ROTATION_SPEED_CALLISTO = 0.008035f;

static constexpr float INITIAL_ORBITAL_ANGLE = glm::radians(30.0f);

sim_control::JovianSystemController::JovianSystemController(const std::shared_ptr<SceneGraph>& sceneGraph)
    : m_sceneGraph(sceneGraph)
    , m_simulationSpeed(100.0f)
    , m_angleIo(INITIAL_ORBITAL_ANGLE)
    , m_angleEuropa(INITIAL_ORBITAL_ANGLE)
    , m_angleGanymede(INITIAL_ORBITAL_ANGLE)
    , m_angleCallisto(INITIAL_ORBITAL_ANGLE)
    , m_angleRotJupiter(0.0f)
    , m_angleRotIo(0.0f)
    , m_angleRotEuropa(0.0f)
    , m_angleRotGanymede(0.0f)
    , m_angleRotCallisto(0.0f)
{
}

void sim_control::JovianSystemController::SetSimulationSpeed(float factor)
{
    m_simulationSpeed = factor;
}

void sim_control::JovianSystemController::Update()
{
    // use the parametric circle equation to generate the orbits, x = r * cost(t), y = r * sin(t)
    // use arc_length = θ × r, where θ is in radians

    float duration = static_cast<float>(m_timer.Restart()); // milliseconds
    duration /= 1000.0f; // seconds

    duration *= m_simulationSpeed;

    SceneNode* pSceneNode = m_sceneGraph->Find(JUPITER);
    if (pSceneNode)
    {
        m_angleRotJupiter += (ROTATION_SPEED_JUPITER * duration / (RADIUS_JUPITER * DISTANCE_FACTOR));
        pSceneNode->SetRotation(m_angleRotJupiter, YAXIS);
    }
    pSceneNode = m_sceneGraph->Find(IO);
    if (pSceneNode)
    {
        m_angleRotIo += (ROTATION_SPEED_IO * duration / (RADIUS_IO * DISTANCE_FACTOR));
        pSceneNode->SetRotation(m_angleRotIo, YAXIS);

        m_angleIo += (ORBITAL_SPEED_IO * duration / (ORBITAL_RADIUS_IO * DISTANCE_FACTOR));
        pSceneNode->SetTranslation(glm::vec3(ORBITAL_RADIUS_IO * glm::cos(m_angleIo),
                                             0,
                                             -ORBITAL_RADIUS_IO * glm::sin(m_angleIo)));
    }
    pSceneNode = m_sceneGraph->Find(EUROPA);
    if (pSceneNode)
    {
        m_angleRotEuropa += (ROTATION_SPEED_EUROPA * duration / (RADIUS_EUROPA * DISTANCE_FACTOR));
        pSceneNode->SetRotation(m_angleRotEuropa, YAXIS);

        m_angleEuropa += (ORBITAL_SPEED_EUROPA * duration / (ORBITAL_RADIUS_EUROPA * DISTANCE_FACTOR));
        pSceneNode->SetTranslation(glm::vec3(ORBITAL_RADIUS_EUROPA * glm::cos(m_angleEuropa),
                                             0,
                                             -ORBITAL_RADIUS_EUROPA * glm::sin(m_angleEuropa)));
    }
    pSceneNode = m_sceneGraph->Find(GANYMEDE);
    if (pSceneNode)
    {
        m_angleRotGanymede += (ROTATION_SPEED_GANYMEDE * duration / (RADIUS_GANYMEDE * DISTANCE_FACTOR));
        pSceneNode->SetRotation(m_angleRotGanymede, YAXIS);

        m_angleGanymede += (ORBITAL_SPEED_GANYMEDE * duration / (ORBITAL_RADIUS_GANYMEDE * DISTANCE_FACTOR));
        pSceneNode->SetTranslation(glm::vec3(ORBITAL_RADIUS_GANYMEDE * glm::cos(m_angleGanymede),
                                             0,
                                             -ORBITAL_RADIUS_GANYMEDE * glm::sin(m_angleGanymede)));
    }
    pSceneNode = m_sceneGraph->Find(CALLISTO);
    if (pSceneNode)
    {
        m_angleRotCallisto += (ROTATION_SPEED_CALLISTO * duration / (RADIUS_CALLISTO * DISTANCE_FACTOR));
        pSceneNode->SetRotation(m_angleRotCallisto, YAXIS);

        m_angleCallisto += (ORBITAL_SPEED_CALLISTO * duration / (ORBITAL_RADIUS_CALLISTO * DISTANCE_FACTOR));
        pSceneNode->SetTranslation(glm::vec3(ORBITAL_RADIUS_CALLISTO * glm::cos(m_angleCallisto),
                                             0,
                                             -ORBITAL_RADIUS_CALLISTO * glm::sin(m_angleCallisto)));
    }

    m_sceneGraph->Update();
}
